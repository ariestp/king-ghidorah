package xzy.monarch.ghidorah.common.channel;


import xzy.monarch.ghidorah.common.transport.EventLoop;

/**
 * @Author: Terrell
 * @Date: 2019-08-06 23:04
 * @Version 1.0
 */
public interface Channel extends ChannelInboundInvoker {


    boolean isOpen();

    boolean isWritealbe();


    boolean registed();

    ChannelFuture register(EventLoop loop);

    String channelId();
}

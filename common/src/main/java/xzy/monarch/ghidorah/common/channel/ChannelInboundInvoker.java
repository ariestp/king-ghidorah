package xzy.monarch.ghidorah.common.channel;

import java.net.SocketAddress;

/**
 * @Author: Terrell
 * @Date: 2019-08-06 23:32
 * @Version 1.0
 */
public interface ChannelInboundInvoker {

    ChannelFuture connect(SocketAddress address);

    ChannelFuture bind(SocketAddress address);

    void  close();

    void disconnect();

    void flush();

    void write(Object msg);

    void writeAndFlush(Object msg);


}

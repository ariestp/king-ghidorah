package xzy.monarch.ghidorah.common.bytebuf;

/**
 * @Author: Terrell
 * @Date: 2019-08-06 23:06
 * @Version 1.0
 */
public interface ByteBuf {
    /**
     * 可读字节数
     *
     * @return
     */
    int readableBytes();

    /**
     * 可写字节数
     *
     * @return
     */
    int writeabeBytes();

    /**
     * 写操作
     *
     * @param bytes
     * @return
     */
    int write(byte[] bytes);

    /**
     * 读操作
     *
     * @param bytes
     * @return
     */
    int read(byte[] bytes);

    /**
     * 清空buff
     */
    void clear();

    /**
     *
     * @param begin
     * @param offset
     * @param bytes
     * @return
     */
    int read(int begin,int offset,byte[] bytes);

    /**
     * 容器最大长度
     * @return
     */
    int maxCapacity();

    /**
     * 可继续写长度
     * @return
     */
    int capacity();


    boolean writeable();


    boolean readable();

    /**
     * 返回所有可读字节数组
     * @return
     */
    byte[] readBytes();

    /**
     * 返回已经写入的字节数组
     * @return
     */
    byte[] writeBytes();


}

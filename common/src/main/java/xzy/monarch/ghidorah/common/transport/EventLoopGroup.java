package xzy.monarch.ghidorah.common.transport;

import xzy.monarch.ghidorah.common.channel.Channel;
import xzy.monarch.ghidorah.common.channel.ChannelFuture;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;

/**
 * @Author: Terrell
 * @Date: 2019-08-07 00:15
 * @Version 1.0
 */
public interface EventLoopGroup  extends ExecutorService, ScheduledExecutorService {

    ChannelFuture register(Channel channel);

    ChannelFuture deregister(Channel channel);

    EventLoop next();
}

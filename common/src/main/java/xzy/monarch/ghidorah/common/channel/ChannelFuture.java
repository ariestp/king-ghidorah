package xzy.monarch.ghidorah.common.channel;

/**
 * @Author: Terrell
 * @Date: 2019-08-06 23:33
 * @Version 1.0
 */
public interface ChannelFuture {

    Channel channel();

    ChannelFuture addListeners(Object... listeners);

    void sync();
}
